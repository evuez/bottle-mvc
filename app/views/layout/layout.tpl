<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>TITLE</title>
		<link rel="stylesheet" href="/css/default.css">
	</head>
	<body>
		%if flash is not '':
			<div id="flash">{{ flash }}</div>
		%end
		<div id="wrapper">
			%include
		</div>
	</body>
</html>