# -*- coding: utf-8 -*-

from app import server, models
from bottle import template, request, static_file, view


def admin():
	pass

@server.route('/:file#(favicon.ico|humans.txt)#')
def favicon(file):
	return static_file(file, root='app/media/static/misc')

@server.route('/:path#(images|css|js|fonts)\/.+#')
def server_static(path):
	return static_file(path, root='app/media/static')


class MyController(object):
	@view('mycontroller/index')
	def index(self, param=''):
		return dict(flash='flash message with param: {0}'.format(param))

mycontroller = MyController()

## routes list
server.route('/')(mycontroller.index)
server.route('/<param>')(mycontroller.index)


## create @admin decorator