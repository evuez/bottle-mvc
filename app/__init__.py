# -*- coding: utf-8 -*-

from bottle import Bottle, TEMPLATE_PATH

server = Bottle()

TEMPLATE_PATH.append("./app/views/")
TEMPLATE_PATH.remove("./views/")

from app import controllers
